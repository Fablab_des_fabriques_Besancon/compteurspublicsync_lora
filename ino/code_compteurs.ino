#include <CayenneLPP.h>

#include <LoRaWan_APP.h>
#include <Arduino.h>

#include "HT_SSD1306Wire.h"

// Battery voltage measurement
#define VBAT_CTRL GPIO_NUM_37
#define VBAT_ADC  GPIO_NUM_1
const float min_voltage = 3.04;
const float max_voltage = 4.26;
const uint8_t scaled_voltage[100] = {
  254, 242, 230, 227, 223, 219, 215, 213, 210, 207,
  206, 202, 202, 200, 200, 199, 198, 198, 196, 196,
  195, 195, 194, 192, 191, 188, 187, 185, 185, 185,
  183, 182, 180, 179, 178, 175, 175, 174, 172, 171,
  170, 169, 168, 166, 166, 165, 165, 164, 161, 161,
  159, 158, 158, 157, 156, 155, 151, 148, 147, 145,
  143, 142, 140, 140, 136, 132, 130, 130, 129, 126,
  125, 124, 121, 120, 118, 116, 115, 114, 112, 112,
  110, 110, 108, 106, 106, 104, 102, 101, 99, 97,
  94, 90, 81, 80, 76, 73, 66, 52, 32, 7,
};

unsigned long delayTime;

#define btnUp 47
#define btnDown 48
boolean UpSwitchReleased;
boolean DownSwitchReleased;

int entrees = 0;
int sorties = 0;
int compteurlocal = 0;
String totalsite = "";

String downstring = "";
char character;

unsigned long currentTime = 0;
unsigned long previousTime = 0;
const int interval = 20000;

/* OTAA para*/
uint8_t devEui[] = { 0x22, 0x32, 0x33, 0x00, 0x00, 0x88, 0x88, 0x01 };
uint8_t appEui[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
uint8_t appKey[] = { 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88 };

/* ABP para*/
uint8_t nwkSKey[] = { 0x15, 0xb1, 0xd0, 0xef, 0xa4, 0x63, 0xdf, 0xbe, 0x3d, 0x11, 0x18, 0x1e, 0x1e, 0xc7, 0xda, 0x85 };
uint8_t appSKey[] = { 0xd7, 0x2c, 0x78, 0x75, 0x8c, 0xdc, 0xca, 0xbf, 0x55, 0xee, 0x4a, 0x77, 0x8d, 0x16, 0xef, 0x67 };
uint32_t devAddr = (uint32_t)0x007e6ae1;

/*LoraWan channelsmask*/
uint16_t userChannelsMask[6] = { 0x00FF, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 };

/*LoraWan region, select in arduino IDE tools*/
LoRaMacRegion_t loraWanRegion = ACTIVE_REGION;

/*LoraWan Class, Class A and Class C are supported*/
DeviceClass_t loraWanClass = CLASS_C;  // Class C enlève le deepsleep

/*the application data transmission duty cycle.  value in [ms].*/
uint32_t appTxDutyCycle = 1500;

/*OTAA or ABP*/
bool overTheAirActivation = true;

/*ADR enable*/
bool loraWanAdr = true;


/* Indicates if the node is sending confirmed or unconfirmed messages */
bool isTxConfirmed = true;

/* Application port */
uint8_t appPort = 2;
/*!
* Number of trials to transmit the frame, if the LoRaMAC layer did not
* receive an acknowledgment. The MAC performs a datarate adaptation,
* according to the LoRaWAN Specification V1.0.2, chapter 18.4, according
* to the following table:
*
* Transmission nb | Data Rate
* ----------------|-----------
* 1 (first)       | DR
* 2               | DR
* 3               | max(DR-1,0)
* 4               | max(DR-1,0)
* 5               | max(DR-2,0)
* 6               | max(DR-2,0)
* 7               | max(DR-3,0)
* 8               | max(DR-3,0)
*
* Note, that if NbTrials is set to 1 or 2, the MAC will not decrease
* the datarate, in case the LoRaMAC layer did not receive an acknowledgment
*/
uint8_t confirmedNbTrials = 4;


CayenneLPP lpp(160);

extern SSD1306Wire display;

float heltec_vbat() {
  pinMode(VBAT_CTRL, OUTPUT);
  digitalWrite(VBAT_CTRL, LOW);
  delay(5);
  float vbat = analogRead(VBAT_ADC) / 238.7;
  // pulled up, no need to drive it
  pinMode(VBAT_CTRL, INPUT);
  return vbat;
}

int heltec_battery_percent(float vbat = -1) {
  if (vbat == -1) {
    vbat = heltec_vbat();
  }
  for (int n = 0; n < sizeof(scaled_voltage); n++) {
    float step = (max_voltage - min_voltage) / 256;
    if (vbat > min_voltage + (step * scaled_voltage[n])) {
      return 100 - n;
    }
  }
  return 0;
}

void updateDisplay() {
  display.end();
  display.init();
  display.screenRotate(ANGLE_180_DEGREE);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_16);
  display.drawString(0, 0, "Compteur=");
  display.drawString(85, 0, String(compteurlocal));
  display.drawString(0, 22, "Total site=");
  display.drawString(85, 22, totalsite);

  float vbat = heltec_vbat();
  int batpercent = heltec_battery_percent(vbat);

  display.drawString(0, 44, "Batterie=");
  display.drawString(85, 44, String(batpercent)+"%");
  display.display();
}

void downLinkAckHandle()
{
   printf("Downlink Ack");
   Serial.println("ack");
}

//downlink data handle function example
void downLinkDataHandle(McpsIndication_t *mcpsIndication)
{
  Serial.printf("+REV DATA:%s,RXSIZE %d,PORT %d\r\n",mcpsIndication->RxSlot?"RXWIN2":"RXWIN1",mcpsIndication->BufferSize,mcpsIndication->Port);
  Serial.print("+REV DATA:");
  for(uint8_t i=0;i<mcpsIndication->BufferSize;i++)
  {
    Serial.printf("%02X",mcpsIndication->Buffer[i]);
  }
  Serial.println();
  for(uint8_t i=0;i<mcpsIndication->BufferSize;i++)
  {
    character = mcpsIndication->Buffer[i];
    downstring.concat(character);
  }
  Serial.print("Total = ");
  Serial.println(downstring);
  totalsite = downstring;
  updateDisplay();
  downstring = "";
}

void compteur() {
  //////////////////////////////////////////////////////
  ////////           BUTTONS                   /////////
  //////////////////////////////////////////////////////

  while (1) {
    currentTime = millis();

    if (currentTime - previousTime >= interval) {
      previousTime = currentTime;
      Serial.println("On prepare la frame");
      prepareTxFrame( appPort );
      break;
    }
    if (digitalRead(btnUp) == 0 && UpSwitchReleased == true) {
      Serial.println("UP");
      UpSwitchReleased = false;
      entrees++;
      compteurlocal=entrees-sorties;
      updateDisplay();
      delay(50);
    }

    else if (digitalRead(btnUp) == 1 && UpSwitchReleased == false) {
      UpSwitchReleased = true;
      delay(50);
    }

    if (digitalRead(btnDown) == 0 && DownSwitchReleased == true) {
      Serial.println("DOWN");
      DownSwitchReleased = false;
      sorties++;
      compteurlocal=entrees-sorties;
      updateDisplay();
      delay(50);
    }

    else if (digitalRead(btnDown) == 1 && DownSwitchReleased == false) {
      DownSwitchReleased = true;
      delay(50);
    }
  }

}

void reinit_compteur() {
  compteurlocal = 0;
  entrees = 0;
  sorties = 0;
  //updateDisplay();
}


/* Prepares the payload of the frame */
static void prepareTxFrame(uint8_t port) {


      lpp.reset();
      lpp.addAnalogInput(1, entrees);
      lpp.addAnalogInput(2, sorties);

      appDataSize = lpp.getSize();
      memcpy(appData, lpp.getBuffer(), lpp.getSize());

}

RTC_DATA_ATTR bool firstrun = true;

void setup() {
  Serial.begin(115200);
  pinMode(Vext, OUTPUT);
  pinMode(btnUp, INPUT_PULLUP);
  pinMode(btnDown, INPUT_PULLUP);
  UpSwitchReleased = true;
  DownSwitchReleased = true;
  Mcu.begin(HELTEC_BOARD,SLOW_CLK_TPYE);

  if (firstrun) {

    LoRaWAN.displayMcuInit();
    firstrun = false;
  }
  deviceState = DEVICE_STATE_INIT;
}
void loop() {
  switch (deviceState) {
    case DEVICE_STATE_INIT:
      {
#if (LORAWAN_DEVEUI_AUTO)
        LoRaWAN.generateDeveuiByChipID();
#endif
        LoRaWAN.init(loraWanClass, loraWanRegion);
        break;
      }
    case DEVICE_STATE_JOIN:
      {
        //LoRaWAN.displayJoining();
        LoRaWAN.join();
        break;
      }
    case DEVICE_STATE_SEND:
      {
        //LoRaWAN.displaySending();
        Serial.println("Preparation de la frame...");
        //prepareTxFrame(appPort);
        compteur();
        Serial.println("On envoie ls valeur du compteur local : ");
        Serial.print(" -- nbre entrees = ");
        Serial.println(entrees);
        Serial.print(" -- nbre sorties = ");
        Serial.println(sorties);
        LoRaWAN.send();
        Serial.println("RAZ compteur");
        reinit_compteur();
        Serial.println("passe en state cycle");
        deviceState = DEVICE_STATE_CYCLE;
        break;
      }
    case DEVICE_STATE_CYCLE:
      {
        // Schedule next packet transmission
        txDutyCycleTime = appTxDutyCycle + randr(0, APP_TX_DUTYCYCLE_RND);
        //txDutyCycleTime = 500;
        LoRaWAN.cycle(txDutyCycleTime);
        Serial.println("lorawan cycle execute");
        deviceState = DEVICE_STATE_SLEEP;
        break;
      }
    case DEVICE_STATE_SLEEP:
      {
        //LoRaWAN.displayAck();
        LoRaWAN.sleep(loraWanClass);
        break;
      }
    default:
      {
        deviceState = DEVICE_STATE_INIT;
        break;
      }
  }
}
