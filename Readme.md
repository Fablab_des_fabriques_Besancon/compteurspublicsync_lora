# Compteurs de public synchronisés, version LoRa

## Système de comptage distribué, sans fil pour les manifestations à jauge limitée

Ce système de boitiers synchronisés permet de connaître en temps réél le nombre de personnes présentes dans un espace à jauge limitée, et comportant plusieurs entrées et sorties. 
À chaque point d'entrée du public sur un site, une personne décompte à l'aide des deux boutons d'un boitier les personnes entrant et les personnes sortant.

Cette version des compteurs est une version LoRa des compteurs https://framagit.org/Fablab_des_fabriques_Besancon/compteurspublicsync qui utilisait la communication ESPNow.
Elle permet une plus grande couverture en distance et l'export des données vers des serveurs de base de données.

![](doc/diagramme.png)

## En pratique

Le système de comptage se compose de 2 types de dispositifs :
* la passerelle réseau (LoRa / LoRaWaN / Wifi / 4G)
* les compteurs

La passerelle permet :
* de récolter les données des compteurs via le réseau LoRa et de les traiter et les consigner dans des fichiers tableurs
* d’envoyer les données comptabilisées à un serveur de base de données temporelles via le réseau 4G
* de créer un point d’accès Wi-Fi pour accéder à l’interface web de gestion des compteurs afin de :
  * voir le total sur site et le détail des entrées/sorties par compteur
  * remettre tous les compteurs à 0 si besoin
  * télécharger les fichiers tableur des enregistrements du total ou des compteurs afin de pouvoir faire des statistiques détaillées

Les compteurs affichent :
* un compteur temporaire des entrées/sorties qui sont envoyées toutes les 20 à 30s
* le total de présence sur site
* l’état de charge de la batterie

### Hardware

La passerelle est une **Dragino LPS8v2** [1] qui possède les connectivités LoRa, WIFI, 4G.

Les compteurs sont basés sur les cartes **Heltec Lora 32 V3** [2]. L'écran OLED couleur intégré et la gestion de la batterie font qu'il n'y a presque rien à ajouter pour finaliser le projet. Une pcb a tout de même été crée avec le logiciel libre KiCad pour faciliter l'assemblage et l'ajout des boutons et interrupteur.

Une batterie 1100 mAh assure une autonomie d'environ 10h des compteurs.

Le boitier a été modélisé à l'aide du logiciel libre FreeCAD et imprimé en PLA.

![](img/compteurs.jpg)

### Software

La passerelle utilise un système d'exploitation opensource fourni par Dragino et basé sur **Armbian** avec un serveur LoRaWAN **Chirpstack** [3] ainsi que l'environnement de développement **Node-RED** [4].

Les compteurs sont programmés avec l'IDE Arduino et utilisent entre autres les librairies Heltec_ESP32 [5] et CayenneLPP [6]

[1] http://wiki.dragino.com/xwiki/bin/view/Main/User%20Manual%20for%20All%20Gateway%20models/HP0C/

[2] https://heltec.org/project/wifi-lora-32-v3/

[3] https://www.chirpstack.io/

[4] https://nodered.org/

[5] https://github.com/HelTecAutomation/Heltec_ESP32

[6] https://github.com/myDevicesIoT/CayenneLPP
